function filter(elements, cb) {
	let status;
	newArray = [];
	for (let element of elements) {
		status = cb(element);
		if (status) {
			newArray.push(element);
		}
	}
	return newArray;
}

const cb = element => {
	if (element % 2) {
		return true;
	} else {
		return false;
	}
}

module.exports = { filter, cb };
