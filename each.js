function each(elements, cb) {
	for (let index in elements) {
		cb(elements[index], index);
	}
	return undefined;
}

const cb = (element, index) => console.log(`${element}: ${index}`);


module.exports = { each, cb }
