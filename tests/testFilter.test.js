const items = require("../items");
const filter = require("../filter");
const { expect } = require("@jest/globals");

const expectedOutput = [1, 3, 5, 5];

test("Will filter out the even numbers", () => {
	expect(filter.filter(items, filter.cb)).toStrictEqual(expectedOutput);
});
