const data = require("../flattenData");
const flatten = require("../flatten");
const { expect } = require("@jest/globals");

const expectedOutput = [1, 2, 3, 4];

test("Will flatten the array", () => {
	expect(flatten(data)).toStrictEqual(expectedOutput);
});
