const items = require("../items");
const find = require("../find");
const { expect } = require("@jest/globals");

const expectedOutput = 5;

test("Will find out the number and give out boolean", () => {
	expect(find.find(items, find.cb)).toStrictEqual(expectedOutput);
});
