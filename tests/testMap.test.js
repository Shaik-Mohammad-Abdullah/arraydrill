const items = require("../items");
const map = require("../map");
const { expect } = require("@jest/globals");

const expectedOutput = [2, 4, 6, 8, 10, 10];

test("Will flatten the array", () => {
	expect(map.map(items, map.cb)).toStrictEqual(expectedOutput);
});
