const items = require("../items");
const reduce = require("../reduce");
const { expect } = require("@jest/globals");
const expectedOutput = 20;

test("Will flatten the array", () => {
	expect(reduce.reduce(items, reduce.cb, 0)).toStrictEqual(expectedOutput);
});
