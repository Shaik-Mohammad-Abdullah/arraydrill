function find(elements, cb) {
	let status;
	let searchedElement = 5;
	for (let element of elements) {
		status = cb(searchedElement, element);
		if (status) {
			return element;
		}
	}
	return undefined;
}

const cb = (findElement, element) => {
	if (findElement === element) {
		return true;
	}
}

module.exports = { find, cb };
