function flatten(elements) {
	let flattenArray = [];
	for (let mainElement of elements) {
		if (Array.isArray(mainElement)) {
			let res = flatten(mainElement);
			for (let element of res) {
				flattenArray.push(element);
			}
		} else {
			flattenArray.push(mainElement);
		}
	}
	return flattenArray;

}

module.exports = flatten;
