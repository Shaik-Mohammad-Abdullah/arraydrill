function map(elements, cb) {
	let newArr = [];
	for (let element of elements) {
		newArr.push(cb(element));
	}
	return newArr;
}

const cb = (element) => element * 2;

module.exports = { map, cb };
