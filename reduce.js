function reduce(elements, cb, startingValue) {
	let index = 0;
	if (startingValue === undefined) {
		startingValue = elements[index];
		index++;
	}
	for (; index < elements.length; index++) {
		startingValue = cb(startingValue, elements[index]);
	}
	return startingValue;
}

const cb = (startingValue, currentElement) => startingValue + currentElement;

module.exports = { reduce, cb };
